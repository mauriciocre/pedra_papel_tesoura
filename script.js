let maquinaEscolha;
let numberPC = (Math.ceil(Math.random() * 3));

// 1 = tesoura
// 2 = papel
// 3 = pedra

let newElement = document.createElement("h2")
document.body.appendChild(newElement);
newElement.style.textAlign = "center"

let resultado = document.createElement("h2")
document.body.appendChild(resultado);
resultado.style.textAlign = "center";


// let imgCorrect = document.createElement("img")
// document.body.appendChild(imgCorrect);
// imgCorrect.src = "correct.png"
// imgCorrect.style.width = "30px";
// imgCorrect.style.height = "30px";


// let imgIncorrect = document.createElement("img")
// document.body.appendChild(imgIncorrect);
// imgCorrect.src = "incorrect.png"
// imgIncorrect.style.width = "30px";
// imgIncorrect.style.height = "30px";


let correct = document.getElementById("correct");
let incorrect = document.getElementById("incorrect");
let draw = document.getElementById("draw");
let imagens = document.getElementsByClassName("imagens");




let contadorHumano = document.getElementById("contadorHumano");
let contadorPC = document.getElementById("contadorPC");

contadorHumano.textContent = 0;
contadorPC.textContent = 0;

function pedra() {
    correct.style.display = "none";
    incorrect.style.display = "none";
    draw.style.display = "none";

    let numberPC = (Math.ceil(Math.random() * 3));
    console.log(numberPC);
    if (numberPC === 1) {
        contadorHumano.textContent = parseInt(contadorHumano.textContent) + 1;
        correct.style.display = "inline";
        newElement.textContent = "Computador escolheu tesoura..."
        resultado.textContent = "Você ganhou!";

    }

    if (numberPC === 2) {
        contadorPC.textContent = parseInt(contadorPC.textContent) + 1;
        incorrect.style.display = "inline";
        newElement.textContent = "Computador escolheu papel...";
        resultado.textContent = "Você perdeu!";
    }
    if (numberPC === 3) {
        draw.style.display = "inline";
        newElement.textContent = "Computador também escolheu pedra..."
        resultado.textContent = "Empate!"
    }
}

function papel() {
    correct.style.display = "none";
    incorrect.style.display = "none";
    draw.style.display = "none";
    let numberPC = (Math.ceil(Math.random() * 3));
    if (numberPC === 1) {
        contadorPC.textContent = parseInt(contadorPC.textContent) + 1;
        incorrect.style.display = "inline";
        newElement.textContent = "Computador escolheu tesoura..."
        resultado.textContent = "Você perdeu!";
    }

    if (numberPC === 2) {
        draw.style.display = "inline";
        newElement.textContent = "Computador também escolheu papel..."
        resultado.textContent = "Empate!"
    }
    if (numberPC === 3) {
        contadorHumano.textContent = parseInt(contadorHumano.textContent) + 1;
        correct.style.display = "inline";
        newElement.textContent = "Computador escolheu pedra..."
        resultado.textContent = "Você ganhou!"
    }
}

function tesoura() {
    correct.style.display = "none";
    incorrect.style.display = "none";
    draw.style.display = "none";
    let numberPC = (Math.ceil(Math.random() * 3));
    if (numberPC === 1) {
        draw.style.display = "inline";
        newElement.textContent = "Computador também escolheu tesoura..."
        resultado.textContent = "Empate!"
    }

    if (numberPC === 2) {
        contadorHumano.textContent = parseInt(contadorHumano.textContent) + 1;
        correct.style.display = "inline";
        newElement.textContent = "Computador escolheu papel..."
        resultado.textContent = "Você ganhou!"
    }
    if (numberPC === 3) {
        contadorPC.textContent = parseInt(contadorPC.textContent) + 1;
        incorrect.style.display = "inline";
        newElement.textContent = "Computador escolheu pedra..."
        resultado.textContent = "Você perdeu!";
    }
}